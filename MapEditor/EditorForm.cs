﻿using System;
using System.Windows.Forms;
using DayZ2D.Components;
using Microsoft.Xna.Framework;

namespace MapEditor
{
    public partial class EditorForm : Form
    {
        public EditorForm()
        {
            InitializeComponent();
            mapControl.sheetView = sheetViewControl;
            sheetViewControl.Sheet = "Images/Sheets/SpriteSheet";
            mapControl.Text = "Open a map or createa new one...";
            sheetViewControl.Text = "Choose sprite sheet.";
            
        }

        private void KeyPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                mapControl.Enabled = !mapControl.Enabled;
            }
            if (e.KeyCode == Keys.C)
            {
                mapControl.ShowCollisions = !mapControl.ShowCollisions;
                mapControl.Invalidate();
            }
        }

        private void newToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            var newmap = new NewMapForm();
            newmap.ShowDialog(this);
            if (newmap.DialogResult == DialogResult.OK)
            {
                mapControl.CreateNewMap(newmap.size, newmap.defTile, newmap.spriteSheet);
                sheetViewControl.CreateNewMap(newmap.spriteSheet);
                mapControl.Enabled = true;
                sheetViewControl.Enabled = true;
                layers.Enabled = true;
                mapControl.Invalidate();
                sizeLabel.Text = String.Format("Map size: {0}x{1} tiles", mapControl.map.Size.X, mapControl.map.Size.Y);
            }

        }

        private void layerCol_Click(object sender, EventArgs e)
        {
            mapControl.Invalidate();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.ShowDialog(this);
        }

        private void saveFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dialog = (SaveFileDialog) sender;
            mapControl.map.Save(dialog.FileName);
        }

        private void openFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dialog = (OpenFileDialog)sender;
            
            Map newMap = (Map)Map.Load(dialog.FileName);
            mapControl.CreateNewMap(newMap.Size, Vector2.One, sheetViewControl.Sheet);
            sheetViewControl.CreateNewMap(sheetViewControl.Sheet);
            mapControl.map = newMap;
            mapControl.Enabled = true;
            sheetViewControl.Enabled = true;
            layers.Enabled = true;
            mapControl.Invalidate();
            sizeLabel.Text = String.Format("Map size: {0}x{1} tiles", mapControl.map.Size.X, mapControl.map.Size.Y);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog(this);
        }
    }
}
