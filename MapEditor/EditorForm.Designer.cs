﻿namespace MapEditor
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layerBG = new System.Windows.Forms.RadioButton();
            this.layerFG = new System.Windows.Forms.RadioButton();
            this.layerCol = new System.Windows.Forms.RadioButton();
            this.layers = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rotationLabel = new System.Windows.Forms.Label();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mapControl = new MapEditor.MapControl();
            this.sheetViewControl = new MapEditor.SheetViewControl();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.layers.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1313, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // layerBG
            // 
            this.layerBG.AutoSize = true;
            this.layerBG.Checked = true;
            this.layerBG.Location = new System.Drawing.Point(6, 19);
            this.layerBG.Name = "layerBG";
            this.layerBG.Size = new System.Drawing.Size(83, 17);
            this.layerBG.TabIndex = 3;
            this.layerBG.TabStop = true;
            this.layerBG.Text = "Background";
            this.layerBG.UseVisualStyleBackColor = true;
            this.layerBG.Click += new System.EventHandler(this.layerCol_Click);
            // 
            // layerFG
            // 
            this.layerFG.AutoSize = true;
            this.layerFG.Location = new System.Drawing.Point(103, 19);
            this.layerFG.Name = "layerFG";
            this.layerFG.Size = new System.Drawing.Size(79, 17);
            this.layerFG.TabIndex = 4;
            this.layerFG.Text = "Foreground";
            this.layerFG.UseVisualStyleBackColor = true;
            this.layerFG.Click += new System.EventHandler(this.layerCol_Click);
            // 
            // layerCol
            // 
            this.layerCol.AutoSize = true;
            this.layerCol.Location = new System.Drawing.Point(198, 19);
            this.layerCol.Name = "layerCol";
            this.layerCol.Size = new System.Drawing.Size(66, 17);
            this.layerCol.TabIndex = 5;
            this.layerCol.Text = "Colisions";
            this.layerCol.UseVisualStyleBackColor = true;
            this.layerCol.Click += new System.EventHandler(this.layerCol_Click);
            // 
            // layers
            // 
            this.layers.Controls.Add(this.layerBG);
            this.layers.Controls.Add(this.layerCol);
            this.layers.Controls.Add(this.layerFG);
            this.layers.Enabled = false;
            this.layers.Location = new System.Drawing.Point(530, 548);
            this.layers.Name = "layers";
            this.layers.Size = new System.Drawing.Size(282, 50);
            this.layers.TabIndex = 6;
            this.layers.TabStop = false;
            this.layers.Text = "Paint Layers";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rotationLabel);
            this.groupBox1.Controls.Add(this.sizeLabel);
            this.groupBox1.Location = new System.Drawing.Point(819, 549);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 50);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info";
            // 
            // rotationLabel
            // 
            this.rotationLabel.AutoSize = true;
            this.rotationLabel.Location = new System.Drawing.Point(6, 32);
            this.rotationLabel.Name = "rotationLabel";
            this.rotationLabel.Size = new System.Drawing.Size(157, 13);
            this.rotationLabel.TabIndex = 1;
            this.rotationLabel.Text = "Brush Rotation:  -000 {Degrees)";
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(6, 16);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(144, 13);
            this.sizeLabel.TabIndex = 0;
            this.sizeLabel.Text = "Map Size:  0000x0000 (Tiles)";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "map";
            this.saveFileDialog.FileName = "map";
            this.saveFileDialog.Filter = "DayZ2D Map Files|*.map";
            this.saveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_FileOk);
            // 
            // mapControl
            // 
            this.mapControl.Enabled = false;
            this.mapControl.Location = new System.Drawing.Point(530, 30);
            this.mapControl.Name = "mapControl";
            this.mapControl.Size = new System.Drawing.Size(771, 512);
            this.mapControl.TabIndex = 7;
            // 
            // sheetViewControl
            // 
            this.sheetViewControl.Enabled = false;
            this.sheetViewControl.Location = new System.Drawing.Point(12, 30);
            this.sheetViewControl.Name = "sheetViewControl";
            this.sheetViewControl.Size = new System.Drawing.Size(512, 512);
            this.sheetViewControl.TabIndex = 8;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1313, 619);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.layers);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.mapControl);
            this.Controls.Add(this.sheetViewControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "EditorForm";
            this.Text = "DayZ2D - Map Editor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyPressed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.layers.ResumeLayout(false);
            this.layers.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MapControl mapControl;
        public SheetViewControl sheetViewControl;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.RadioButton layerBG;
        public System.Windows.Forms.RadioButton layerFG;
        public System.Windows.Forms.RadioButton layerCol;
        public System.Windows.Forms.GroupBox layers;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label sizeLabel;
        public System.Windows.Forms.Label rotationLabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}