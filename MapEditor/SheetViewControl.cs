﻿using System;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using DayZ2D.Components;

namespace MapEditor
{
    public struct Selection
    {
        public Vector2 TopLeft;
        public Vector2 BotomRight;

        public int SelectedTiles()
        {
            return (SelectedColoms()*SelectedRows());
        }

        public int SelectedColoms()
        {
            return (int) (BotomRight.X - TopLeft.X + 1);
        }

        public int SelectedRows()
        {
            return (int) (BotomRight.Y - TopLeft.Y + 1);
        }
    }
    public class SheetViewControl : GraphicsDeviceControl
    {
        private Texture2D spriteSheet;
        public string Sheet;
        public Selection Selection;
        private ContentManager content;
        private SpriteBatch spriteBatch;
        private Camera camera;
        private Selector selector;
        private Vector2 clickPoint;
        private bool selecting;
        public bool selected;

        public SheetViewControl()
        {
            content = new ContentManager(Services, "Content");
            selector = new Selector();
            this.KeyDown += SheetViewControl_KeyDown;
            this.MouseEnter += SheetViewControl_MouseEnter;
            this.MouseMove += SheetViewControl_MouseMove;
            this.MouseDown += SheetViewControl_MouseDown;
            this.MouseUp += SheetViewControl_MouseUp;
        }

        public void CreateNewMap(string sheet)
        {
            content = new ContentManager(Services, "Content");
            camera = new Camera();
            camera.Zoom = 1f;
            selector = new Selector();
            Sheet = sheet;
            
            Initialize();
        }

        void SheetViewControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                selecting = false;
            }
            if (e.Button == MouseButtons.Right)
            {
                selected = false;
            }
            ((EditorForm) Parent).rotationLabel.Text = Selection.SelectedTiles().ToString();
        }

        void SheetViewControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var pos = new Vector2(e.X, e.Y);
                pos = camera.ScreenToWorlSpace(pos);
                clickPoint = pos;
                selecting = true;
                SheetViewControl_MouseMove(sender, e);
            }
        }

        void SheetViewControl_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = new Vector2(e.X, e.Y);
            pos = camera.ScreenToWorlSpace(pos);
            if (selecting)
            {
                selector.PositionTL = new Vector2(Math.Min(pos.X, clickPoint.X), Math.Min(pos.Y, clickPoint.Y));
                selector.PositionBR = new Vector2(Math.Max(pos.X, clickPoint.X), Math.Max(pos.Y, clickPoint.Y));

                Selection.TopLeft = new Vector2((float) Math.Floor(selector.PositionTL.X/32), (float) Math.Floor(selector.PositionTL.Y/32));
                Selection.BotomRight = new Vector2((float)Math.Floor(selector.PositionBR.X / 32), (float)Math.Floor(selector.PositionBR.Y / 32));

                Invalidate();
                selected = true;
            }
            if (!selected)
            {
                selector.PositionTL = selector.PositionBR = pos;
                Invalidate();
            }
        }

        void SheetViewControl_MouseEnter(object sender, System.EventArgs e)
        {
            Focus();
        }

        void SheetViewControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                camera.Pos = new Vector2(camera.Pos.X - 32, camera.Pos.Y);
            }
            if (e.KeyCode == Keys.D)
            {
                camera.Pos = new Vector2(camera.Pos.X + 32, camera.Pos.Y);
            }
            if (e.KeyCode == Keys.W)
            {
                camera.Pos = new Vector2(camera.Pos.X, camera.Pos.Y - 32);
            }
            if (e.KeyCode == Keys.S)
            {
                camera.Pos = new Vector2(camera.Pos.X, camera.Pos.Y + 32);
            }
            camera.Pos = new Vector2(MathHelper.Clamp(camera.Pos.X, 0, 512), MathHelper.Clamp(camera.Pos.Y, 0, 512));
            Invalidate();
        }

        protected override void Initialize()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteSheet = content.Load<Texture2D>(Sheet);
            selector.Initialize(content);
        }

        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.DarkGreen);

            spriteBatch.Begin(SpriteSortMode.BackToFront, null, null, null, null, null, camera.getTransform(GraphicsDevice));
            spriteBatch.Draw(spriteSheet, Vector2.Zero, Color.White);
            selector.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
