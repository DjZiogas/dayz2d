﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DayZ2D.Components;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor
{
    public class MapControl : GraphicsDeviceControl
    {
        private Camera camera;
        private SpriteBatch spriteBatch;
        private ContentManager content;
        private Selector selector;
        private Texture2D spriteSheet;
        private Map _map;
        public bool ShowCollisions = true;
        public SheetViewControl sheetView;
        private Stack<Map> history;

        public MapControl()
        {
            content = new ContentManager(Services, "Content");
            selector = new Selector();
            MouseEnter += MapControl_MouseEnter;
            MouseMove += MapControl_MouseMove;
            MouseUp += MapControl_MouseUp;
            MouseDown += MapControl_MouseDown;
            KeyDown += MapControl_KeyDown;
            history = new Stack<Map>();

        }
        public void CreateNewMap(Vector2 size, Vector2 defTile, string spriteTexture)
        {
            camera = new Camera();
            selector = new Selector();
            content = new ContentManager(Services, "Content");
            map = new Map(size, defTile);
            history.Push(map);
            this.spriteSheet = content.Load<Texture2D>(spriteTexture);
            sheetView = ((EditorForm)Parent).sheetViewControl;
            Initialize();
        }

        public Map map { get { return _map; } set { _map = value; _map.LoadContent(content); } }

        void MapControl_MouseUp(object sender, MouseEventArgs e)
        {
            var pos = new Vector2(e.X, e.Y);
            pos = camera.ScreenToWorlSpace(pos);
            if (sheetView.selected && e.Button == MouseButtons.Left && !((EditorForm)Parent).layerCol.Checked)
            {
                for (int i = 0; i < sheetView.Selection.SelectedColoms(); i++)
                {
                    for (int j = 0; j < sheetView.Selection.SelectedRows(); j++)
                    {
                        if (((EditorForm)Parent).layerBG.Checked)
                        {
                            map.SetTile(new Vector2(pos.X + i * 32, pos.Y + j * 32), new Vector2(sheetView.Selection.TopLeft.X + i, sheetView.Selection.TopLeft.Y + j), MapLayer.Background);
                        }
                        else
                        {
                            map.SetTile(new Vector2(pos.X + i * 32, pos.Y + j * 32), new Vector2(sheetView.Selection.TopLeft.X + i, sheetView.Selection.TopLeft.Y + j), MapLayer.Foreground);
                        }

                    }
                }
            }
        }

        void MapControl_MouseDown(object sender, MouseEventArgs e)
        {
            var copy = (Map)map.Clone();
            history.Push(copy);
        }

        void MapControl_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = new Vector2(e.X, e.Y);
            pos = camera.ScreenToWorlSpace(pos);

            if (((EditorForm)Parent).layerCol.Checked)
            {
                if (e.Button == MouseButtons.Left)
                {
                    map.SetCollision(pos, Colides.colides);
                }
                if (e.Button == MouseButtons.Right)
                {
                    map.SetCollision(pos, Colides.none);
                }
                Invalidate();
            }
            else
            {
                if (sheetView.selected)
                {
                    selector.PositionTL = pos;
                    selector.PositionBR = new Vector2(
                        pos.X + (sheetView.Selection.BotomRight.X - sheetView.Selection.TopLeft.X) * 32,
                        pos.Y + (sheetView.Selection.BotomRight.Y - sheetView.Selection.TopLeft.Y) * 32);

                    if (sheetView.Selection.SelectedTiles() == 1)
                    {
                        if (e.Button == MouseButtons.Left && ((EditorForm)Parent).layerBG.Checked)
                        {
                            map.SetTile(pos, sheetView.Selection.TopLeft, MapLayer.Background);
                        }
                    }

                    Invalidate();
                }
            }
        }


        void MapControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                camera.Pos = new Vector2(camera.Pos.X - 32, camera.Pos.Y);
            }
            if (e.KeyCode == Keys.D)
            {
                camera.Pos = new Vector2(camera.Pos.X + 32, camera.Pos.Y);
            }
            if (e.KeyCode == Keys.W)
            {
                camera.Pos = new Vector2(camera.Pos.X, camera.Pos.Y - 32);
            }
            if (e.KeyCode == Keys.S)
            {
                camera.Pos = new Vector2(camera.Pos.X, camera.Pos.Y + 32);
            }
            if (e.KeyCode == Keys.Z && e.Control)
            {
                if (history.Count > 1)
                {
                    var copy = new Map(Vector2.One, Vector2.One);
                    copy = history.Pop();
                    map = copy;
                }
            }
            Invalidate();
        }

        void MapControl_MouseEnter(object sender, EventArgs e)
        {
            Focus();
        }

        protected override void Initialize()
        {
            selector.Initialize(content);

            spriteBatch = new SpriteBatch(GraphicsDevice);
            if (map != null)
                map.LoadContent(content);
        }

        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.DarkGreen);

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.LinearClamp,
                DepthStencilState.None,
                RasterizerState.CullCounterClockwise,
                null,
                camera.getTransform(GraphicsDevice));


            map.Draw(spriteBatch, spriteSheet, ShowCollisions);

            if (!((EditorForm)Parent).layerCol.Checked && sheetView.selected)
            {
                selector.Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
