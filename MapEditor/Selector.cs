﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor
{
    public class Selector
    {
        public Vector2 PositionTL;
        public Vector2 PositionBR;
        private Texture2D texture;
        public Selector()
        {

        }

        public void Initialize(ContentManager content)
        {
            texture = content.Load<Texture2D>("Images/editor/selector");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            PositionTL = new Vector2((float)Math.Floor(PositionTL.X / 32) * 32, (float)Math.Floor(PositionTL.Y / 32) * 32);
            PositionBR = new Vector2((float)Math.Floor(PositionBR.X / 32) * 32, (float)Math.Floor(PositionBR.Y / 32) * 32);

            spriteBatch.Draw(texture, PositionTL, new Rectangle(0, 0, 32, 32), Color.White);
            spriteBatch.Draw(texture, new Vector2(PositionBR.X, PositionTL.Y), new Rectangle(32, 0, 32, 32), Color.White);
            spriteBatch.Draw(texture, PositionBR, new Rectangle(32, 32, 32, 32), Color.White);
            spriteBatch.Draw(texture, new Vector2(PositionTL.X, PositionBR.Y), new Rectangle(0, 32, 32, 32), Color.White);
        }
    }
}