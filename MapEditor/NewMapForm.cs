﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;

namespace MapEditor
{
    public partial class NewMapForm : Form
    {
        public Vector2 size;
        public Vector2 defTile;
        public string spriteSheet;
        public NewMapForm()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            size = new Vector2(Int32.Parse(mapSizeX.Text), Int32.Parse(mapSizeY.Text));
            defTile = new Vector2(Int32.Parse(tileX.Text),Int32.Parse(tileY.Text));
            spriteSheet = spriteSheetName.Text;
        }
    }
}
