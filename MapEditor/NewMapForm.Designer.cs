﻿namespace MapEditor
{
    partial class NewMapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mapSizeY = new System.Windows.Forms.TextBox();
            this.mapSizeX = new System.Windows.Forms.TextBox();
            this.spriteSheetName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tileY = new System.Windows.Forms.TextBox();
            this.tileX = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mapSizeY);
            this.groupBox1.Controls.Add(this.mapSizeX);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Map Size";
            // 
            // mapSizeY
            // 
            this.mapSizeY.Location = new System.Drawing.Point(113, 19);
            this.mapSizeY.Name = "mapSizeY";
            this.mapSizeY.Size = new System.Drawing.Size(100, 20);
            this.mapSizeY.TabIndex = 1;
            // 
            // mapSizeX
            // 
            this.mapSizeX.Location = new System.Drawing.Point(7, 19);
            this.mapSizeX.Name = "mapSizeX";
            this.mapSizeX.Size = new System.Drawing.Size(100, 20);
            this.mapSizeX.TabIndex = 0;
            // 
            // spriteSheetName
            // 
            this.spriteSheetName.Location = new System.Drawing.Point(6, 19);
            this.spriteSheetName.Name = "spriteSheetName";
            this.spriteSheetName.Size = new System.Drawing.Size(206, 20);
            this.spriteSheetName.TabIndex = 2;
            this.spriteSheetName.Text = "Images/Sheets/SpriteSheet";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.spriteSheetName);
            this.groupBox2.Location = new System.Drawing.Point(13, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 51);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sprite sheet path";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(159, 184);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 6;
            this.okButton.Text = "Create";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.CausesValidation = false;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(78, 184);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tileY);
            this.groupBox3.Controls.Add(this.tileX);
            this.groupBox3.Location = new System.Drawing.Point(13, 129);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(221, 49);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Default Tile";
            // 
            // tileY
            // 
            this.tileY.Location = new System.Drawing.Point(112, 19);
            this.tileY.Name = "tileY";
            this.tileY.Size = new System.Drawing.Size(100, 20);
            this.tileY.TabIndex = 4;
            // 
            // tileX
            // 
            this.tileX.Location = new System.Drawing.Point(6, 19);
            this.tileX.Name = "tileX";
            this.tileX.Size = new System.Drawing.Size(100, 20);
            this.tileX.TabIndex = 3;
            // 
            // NewMapForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(248, 219);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewMapForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Map";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox mapSizeY;
        private System.Windows.Forms.TextBox mapSizeX;
        private System.Windows.Forms.TextBox spriteSheetName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tileY;
        private System.Windows.Forms.TextBox tileX;
    }
}