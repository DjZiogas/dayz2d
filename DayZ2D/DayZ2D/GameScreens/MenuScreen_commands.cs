﻿using DayZ2D.Managers;

namespace DayZ2D.GameScreens
{
    public partial class MenuScreen
    {
        public void Play()
        {
            ScreenManager.Instance.ChangeScreens("GameplayScreen");
        }

        public void Exit()
        {
            ScreenManager.Instance.Game.Exit();
        }

        public void Settings()
        {
            if (_settingsMenu.IsHidden)
            {
                _mainMenu.SetHidden(true);
                _settingsMenu.SetHidden(false);
            }
            else
            {
                _settingsMenu.SetHidden(true);
                _mainMenu.SetHidden(false);
            }
        }
    }
}
