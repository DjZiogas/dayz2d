﻿using System.Collections;
using System.Collections.Generic;
using DataTypes;
using DayZ2D.Components;
using DayZ2D.Components.GUI;
using DayZ2D.Components.GUI.Commands;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace DayZ2D.GameScreens
{
    public partial class MenuScreen : BaseGameScreen
    {
        private Song _song;
        private Menu _mainMenu;
        private Menu _newMapMenu;
        private Menu _settingsMenu;

        public override void LoadContent()
        {
            base.LoadContent();
//            _song = Content.Load<Song>("Music/Menu");
//            MediaPlayer.Play(_song);
//            MediaPlayer.IsRepeating = true;
            CreateMenus();
            _mainMenu.LoadContent(Content);
            _settingsMenu.LoadContent(Content);
            _newMapMenu.LoadContent(Content);
            ScreenManager.Instance.Camera.Zoom = 1f;
        }

        private void CreateMenus()
        {
            ICommand playCommand = new PlayCommand(this);
            ICommand exitCommand = new ExitCommand(this);
            ICommand settingsCommand = new SettingsCommand(this);
            IList< Button > mainButtons = new List<Button>
            {
                new Button(playCommand, "play"),
                new Button(settingsCommand, "settings"),
                new Button(exitCommand, "exit")
            };
            IList<Button> settingsButtons = new List<Button>
            {
                new Button(settingsCommand, "back"){Enabled = false},
                new Button(settingsCommand, "back"){Enabled = false},
                new Button(settingsCommand, "back"){Enabled = false}
            };

            _mainMenu = new Menu(mainButtons,false,"DAYZ2D  Menu");
            _settingsMenu = new Menu(settingsButtons, true, "DAYZ2D  Setting");
            _newMapMenu = new Menu(mainButtons,true,"DAYZ2D  New Game");
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (_mainMenu.IsHidden && !_settingsMenu.IsHidden)
            {
                _settingsMenu.Update(gameTime);
                _newMapMenu.Update(gameTime);
            }

            if(_settingsMenu.IsHidden && _newMapMenu.IsHidden && !_mainMenu.IsHidden)
                _mainMenu.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.Clear(Color.Black);
            _mainMenu.Draw(spriteBatch);
            _settingsMenu.Draw(spriteBatch);
            _newMapMenu.Draw(spriteBatch);
        }
    }
}
