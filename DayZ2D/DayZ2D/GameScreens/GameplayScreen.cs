﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using DayZ2D.Components;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

namespace DayZ2D.GameScreens
{
    public partial class GameplayScreen : BaseGameScreen
    {
        private Texture2D texture;
        private Texture2D spriteSheet;
        private AiManager aiManager;
        private Player player;
        private Map _map;
        public override void LoadContent()
        {
            base.LoadContent();
            aiManager = new AiManager();
            aiManager.LoadContent();
            texture = Content.Load<Texture2D>("Images/64");
            spriteSheet = Content.Load<Texture2D>("Images/Sheets/SpriteSheet");
            player = new Player(200, "Images/Zombie/Zombie_move", Vector2.Zero, 0.2f);
            var colManager = DependencyInjectionConfiguration.Container.Resolve<CollisionManager>();
            colManager.AddCollider(player);
            player.LoadContent(Content);
            _map = (Map)Map.Load("Content/Maps/map.map");
            _map.GenerateCollisions();
            _map.LoadContent(Content);
            
            ScreenManager.Instance.Camera.Zoom = 1.5f;
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            aiManager.Update(gameTime);
            player.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.Clear(Color.Black);
            _map.Draw(spriteBatch, spriteSheet, false);
            player.Draw(spriteBatch);
            aiManager.Draw(spriteBatch);
        }
    }
}
