﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DayZ2D.GameScreens
{
    class SplashScreen : BaseGameScreen
    {
        private Texture2D texture;
        private float fadeAlpha = 0.0f;
        private float elapsedTime = 0.0f;
        private float secondsToHold = 3f;
        private bool tempFrameRateSetting;
        public override void LoadContent()
        {
            base.LoadContent();
            texture = Content.Load<Texture2D>("Images/Splashes/Splash");
            tempFrameRateSetting = Configuration.Instance.Settings.ShowFps;
            Configuration.Instance.Settings.ShowFps = false;
        }

        public override void Update(GameTime gameTime)
        {
            elapsedTime += (float) gameTime.ElapsedGameTime.TotalSeconds;
            if (elapsedTime >= secondsToHold)
            {
                if (fadeAlpha > 0.0f)
                {
                    fadeAlpha -= (float) gameTime.ElapsedGameTime.TotalSeconds;
                    if (fadeAlpha <= 0.0f)
                    {
                        switchOffSplash();
                    }
                }
            }
            else if (fadeAlpha < 1.0f)
            {
                fadeAlpha += (float) gameTime.ElapsedGameTime.TotalSeconds;
                if (fadeAlpha >= 1.0f) fadeAlpha = 1.0f;
            }
            if (InputManager.Instance.KeyPresed(Keys.Escape))
            {
                switchOffSplash();
            }
            base.Update(gameTime);
        }

        private void switchOffSplash()
        {
            ScreenManager.Instance.ChangeScreens("MenuScreen");
            ScreenManager.Instance.Cursor.IsEnabled = true;
            Configuration.Instance.Settings.ShowFps = tempFrameRateSetting;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture,new Rectangle(0,0, spriteBatch.GraphicsDevice.Viewport.Width, spriteBatch.GraphicsDevice.Viewport.Height), new Color(new Vector4(fadeAlpha)));
        }
    }
}
