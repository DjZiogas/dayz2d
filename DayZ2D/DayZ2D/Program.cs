using System;
using DayZ2D.Helpers;

namespace DayZ2D
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            DependencyInjectionConfiguration.Configure();
            using (Game game = new Game())
            {
                game.Run();
            }
        }
    }
#endif
}

