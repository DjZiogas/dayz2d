using System;
using System.INI;
using Autofac;
using DayZ2D.Components;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XNAGameConsole;

namespace DayZ2D
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        private CollisionManager _colManager;
        GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        private float _secToHold = 2f;
        public GameConsole _console;
        public Game()
        {
            Configuration.Instance.Load();
            /////// graphix settings
            this.IsFixedTimeStep = Configuration.Instance.Settings.FrameRateLock;
            var ini = new IniFile(@Environment.CurrentDirectory+"\\settings.ini");
            _graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = Configuration.Instance.Settings.ScreenWidth,
                PreferredBackBufferHeight = Configuration.Instance.Settings.ScreenHeight,
                IsFullScreen = Configuration.Instance.Settings.FullScreen,
                SynchronizeWithVerticalRetrace = Configuration.Instance.Settings.VSync
            };
            ///////////////////////////////////////////
            Content.RootDirectory = "Content";
            _colManager = DependencyInjectionConfiguration.Container.Resolve<CollisionManager>();
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            ScreenManager.Instance.Game = this;
            ScreenManager.Instance.LoadContent(Content, GraphicsDevice);
            InputManager.Instance.LoadContent();
            _console = new GameConsole(this, _spriteBatch);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            InputManager.Instance.UnloadContent();
            ScreenManager.Instance.UnloadContent();
            Configuration.Instance.Save();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (InputManager.Instance.KeyHolding(Keys.Escape))
            {
                _secToHold -= (float) gameTime.ElapsedGameTime.TotalSeconds;
                if (_secToHold <= 0)
                {
                    Configuration.Instance.Save();
                    this.Exit();
                }
            }
            else
            {
                _secToHold = 2;
            }

            if (InputManager.Instance.KeyPresed(Keys.Up))
            {
                Configuration.Instance.Settings.MusicVolume += 0.1f;
            }
            if (InputManager.Instance.KeyPresed(Keys.Down))
            {
                Configuration.Instance.Settings.MusicVolume -= 0.1f;
            }
            InputManager.Instance.Update(gameTime);
            ScreenManager.Instance.Update(gameTime);
            _colManager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            ScreenManager.Instance.Draw(_spriteBatch, gameTime);


            base.Draw(gameTime);
        }
    }
}
