﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Managers;

namespace DayZ2D.Logger
{
    public class DebugLogger : AbstractLogger
    {
        public DebugLogger(int level)
        {
            Level = level;
        }
        protected override void Write(string message)
        {
            ScreenManager.Instance.Game._console.WriteLine("DEBUG: " + message);
        }
    }
}
