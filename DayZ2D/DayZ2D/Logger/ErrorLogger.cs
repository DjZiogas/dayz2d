﻿using System;
using DayZ2D.Managers;

namespace DayZ2D.Logger
{
    public class ErrorLogger : AbstractLogger
    {
        public ErrorLogger(int level)
        {
            Level = level;
        }

        protected override void Write(string message)
        {
            ScreenManager.Instance.Game._console.WriteLine("ERROR: " + message);
        }
    }
}
