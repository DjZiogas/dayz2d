﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DayZ2D.Logger
{
    public abstract class AbstractLogger
    {
       public static int INFO = 1;
       public static int DEBUG = 2;
       public static int ERROR = 3;

       protected int Level;

       //next element in chain or responsibility
       protected AbstractLogger NextLogger;

       public void SetNextLogger(AbstractLogger nextLogger)
       {
          NextLogger = nextLogger;
       }

       public void LogMessage(int level, string message)
       {
          if(Level <= level)
          {
             Write(message);
          }
          if(NextLogger != null)
          {
             NextLogger.LogMessage(level, message);
          }
       }

       abstract protected void Write(string message);

    }
}
