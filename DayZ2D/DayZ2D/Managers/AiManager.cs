﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using DayZ2D.Components;
using DayZ2D.Components.AI;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Managers
{
    class AiManager
    {
        private List<IBaseEnemy> _enemies;
        private EnemyProducer _producer;

        public AiManager()
        {
            _producer = new EnemyProducer();
            _enemies = new List<IBaseEnemy>();
        }

        public void LoadContent()
        {
            _enemies.Add(EnemyProducer.CreateEnemy(Enemy.Zombie).Spawn(EnemyTypes.Lite, new Vector2(200,200)));
        }

        public void Update(GameTime gameTime)
        {
            foreach (var enemy in _enemies)
            {
                enemy.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var enemy in _enemies)
            {
                enemy.Draw(spriteBatch);
            }
        }
    }
}