﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Components;
using DayZ2D.GameScreens;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Managers
{
    public class ScreenManager
    {
        private static readonly ScreenManager _instance = new ScreenManager();
        public static ScreenManager Instance { get { return _instance; } }
        public ContentManager Content { private set; get; }

        private Viewport _viewport;
        public Viewport Viewport { get { return _viewport; } }

        private Game _game;
        public Game Game { get { return _game; } set { _game = value; } }

        private BaseGameScreen _currentScreen;
        private Camera _camera;
        private FrameRateCounter _fpsCounter;

        public Camera Camera
        {
            get { return _camera; }
            private set { _camera = value; }
        }

        private Cursor _cursor;
        public Cursor Cursor { get { return _cursor; } set { _cursor = value; } }

        public ScreenManager()
        {
            _fpsCounter = new FrameRateCounter();
            _currentScreen = new SplashScreen();
            _cursor = new Cursor("Images/croshair");
            _camera = new Camera();
        }

        public void ChangeScreens(String screen)
        {
            _currentScreen.UnloadContent();
            _currentScreen = (BaseGameScreen) Activator.CreateInstance(Type.GetType("DayZ2D.GameScreens."+screen));
            _currentScreen.LoadContent();
        }
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            Content = new ContentManager(content.ServiceProvider,"Content");

            _currentScreen.LoadContent();
            _cursor.LoadContent(Content);
            _fpsCounter.LaodContent(Content);
        }

        public void UnloadContent()
        {
            _currentScreen.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            _cursor.Update(gameTime);
            _currentScreen.Update(gameTime);
            _fpsCounter.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            _viewport = spriteBatch.GraphicsDevice.Viewport;
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.None, RasterizerState.CullNone, null, _camera.getTransform(spriteBatch.GraphicsDevice));

            _currentScreen.Draw(spriteBatch);

            spriteBatch.End();


            spriteBatch.Begin();
            _fpsCounter.Draw(spriteBatch);
            _cursor.Draw(spriteBatch);
            spriteBatch.End();
        }

    }
}
