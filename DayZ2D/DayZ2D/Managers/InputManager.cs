﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DayZ2D.Managers
{
    public class InputManager
    {
        private static InputManager _instance = new InputManager();

        private KeyboardState currentKeyboardState, previousKeyboardState;
        private MouseState currentMouseState, previousMouseState;

        public static InputManager Instance { get{return _instance;} }

        public void LoadContent()
        {
            
        }

        public void UnloadContent()
        {
            
        }

        public void Update(GameTime gameTime)
        {
            previousKeyboardState = currentKeyboardState;
            previousMouseState = currentMouseState;
            currentKeyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();
        }

        public bool KeyPresed(Keys key)
        {
            if (previousKeyboardState.IsKeyUp(key) && currentKeyboardState.IsKeyDown(key))
                return true;
            return false;
        }

        public bool KeyReleased(Keys key)
        {
            if (previousKeyboardState.IsKeyDown(key) && currentKeyboardState.IsKeyUp(key))
                return true;
            return false;
        }

        public bool KeyHolding(Keys key)
        {
            if (previousKeyboardState.IsKeyDown(key) && currentKeyboardState.IsKeyDown(key))
                return true;
            return false;
        }

        public bool MousePresed()
        {
            return false;
        }

        public bool MouseReleased()
        {
            if (previousMouseState.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
                return true;
            return false;
        }

        public bool MouseHolding()
        {
            return false;
        }
    }
}
