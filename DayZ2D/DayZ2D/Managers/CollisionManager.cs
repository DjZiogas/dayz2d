﻿using System.Collections.Generic;
using Autofac;
using DayZ2D.Components.Collisions;
using DayZ2D.Helpers;
using DayZ2D.Logger;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace DayZ2D.Managers
{
    public class CollisionManager
    {

        private List<ICollidableObject> _coliders;

        public CollisionManager()
        {
            _coliders = new List<ICollidableObject>();
        }

        public void AddCollider(ICollidableObject obj)
        {
            _coliders.Add(obj);
        }

        public void Update(GameTime gameTime)
        {
            foreach (var collidableObject in _coliders)
            {
                if (!collidableObject.Movable) continue;
                foreach (var colider in _coliders)
                {
                    if (collidableObject.Intersects(colider) && colider != collidableObject)
                    {
                        collidableObject.Position = collidableObject.PreviousPosition;
                    }
                }
            }
        }
    }
}
