﻿namespace DayZ2D.Helpers
{
    public enum EnemyTypes
    {
        Lite,
        Normal,
        Heavy
    }

    public enum Enemy
    {
        Bandit,
        Zombie
    }

    public enum ZombieAnimations
    {
        Idle,
        Attack,
        Move
    }

    public enum BanditAnimations
    {
        Idle
    }
}