﻿using System;
using System.Collections.Generic;
using System.INI;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DayZ2D.Helpers
{
    public class Settings
    {
        public int ScreenWidth { get; set; }
        public int ScreenHeight { get; set; }
        public bool FullScreen { get; set; }
        public bool VSync { get; set; }
        public float CursorScale { get; set; }
        private float _volume;
        public float MusicVolume { get { return _volume; } set { _volume = MediaPlayer.Volume = value; } }
        public float SoundVolume { get; set; }
        public bool FrameRateLock { get; set; }
        public bool ShowFps { get; set; }

        public Settings()
        {
            //Default values
            ScreenWidth = 640;
            ScreenHeight = 480;
            FullScreen = false;
            VSync = false;
            CursorScale = 0.5f;
            MusicVolume = 0.5f;
            SoundVolume = 1f;
            FrameRateLock = false;
            ShowFps = false;
        }
    }

    public class Controls
    {
        public Keys WalkLeft { get; set; }
        public Keys WalkRight { get; set; }
        public Keys WalkUp { get; set; }
        public Keys WalkDown { get; set; }
        public Keys Sprint { get; set; }
        public Keys PlantBomb { get; set; }
        public Keys Atack { get; set; }

        public Controls()
        {
            //Default values
            WalkLeft = Keys.A;
            WalkRight = Keys.D;
            WalkUp = Keys.W;
            WalkDown = Keys.S;
            Sprint = Keys.LeftShift;
            PlantBomb = Keys.F;
            Atack = Keys.Space;
        }

    }

    public class Configuration
    {
        private static Configuration _instance = new Configuration();
        [XmlIgnore]
        public static Configuration Instance { get { return _instance; } }

        public Settings Settings;
        public Controls Controls;

        public Configuration()
        {
                Settings = new Settings();
                Controls = new Controls();
        }

        public void Load()
        {
            if (File.Exists("Configuration.xml"))
            {
                using (TextReader reader = new StreamReader("Configuration.xml"))
                {
                    var xml = new XmlSerializer(this.GetType());
                    _instance = (Configuration) xml.Deserialize(reader);
                }
            }
        }

        public void Save()
        {
            using (TextWriter writer = new StreamWriter("Configuration.xml"))
            {
                var xml = new XmlSerializer(this.GetType());
                xml.Serialize(writer,this);
            }
        }
    }
}
