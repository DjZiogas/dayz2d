﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Helpers
{
    class FrameRateCounter
    {
        private int _totalFrames;
        private int _fps;
        private float _elapsedTime;

        private SpriteFont spriteFont;

        public void LaodContent(ContentManager content)
        {
            spriteFont = content.Load<SpriteFont>("Fonts/technical");
        }

        public void Update(GameTime gameTime)
        {
            if (Configuration.Instance.Settings.ShowFps)
            {
                _elapsedTime += (float) gameTime.ElapsedGameTime.TotalMilliseconds;

                if (_elapsedTime >= 1000.0f)
                {
                    _fps = _totalFrames;
                    _totalFrames = 0;
                    _elapsedTime = _elapsedTime - 1000.0f;
                }
            }
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            if (Configuration.Instance.Settings.ShowFps)
            {
                _totalFrames++;
                spriteBatch.DrawString(spriteFont, "fps: " + _fps, new Vector2(20, 20), Color.Yellow);
            }
        }
    }
}
