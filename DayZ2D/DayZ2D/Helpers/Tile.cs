﻿using System;

namespace DayZ2D.Helpers
{
    [Serializable]
    public struct Tile
    {
        public int SheetX;
        public int SheetY;
        public byte Rotation;
    }
}