﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using DayZ2D.Components.AI.Builders;
using DayZ2D.Components.Factories;
using DayZ2D.Logger;
using DayZ2D.Managers;

namespace DayZ2D.Helpers
{
    public class DependencyInjectionConfiguration
    {
        public static IContainer Container { get; private set; }
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            var colManager = new CollisionManager();
            AbstractLogger infoLogger = new InfoLogger(AbstractLogger.INFO);
            AbstractLogger debugLogger = new DebugLogger(AbstractLogger.DEBUG);
            AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);

            errorLogger.SetNextLogger(debugLogger);
            debugLogger.SetNextLogger(infoLogger);

            builder.RegisterInstance(errorLogger).As<AbstractLogger>();
            
            builder.RegisterInstance(colManager).As<CollisionManager>();
            builder.RegisterType<BanditBuilder>().As<BanditBuilder>().InstancePerLifetimeScope();

            builder.RegisterType<BanditFactory>().As<BanditFactory>().InstancePerLifetimeScope();
            builder.RegisterType<ZombieFactory>().As<ZombieFactory>().InstancePerLifetimeScope();



            Container = builder.Build();
        } 
    }
}
