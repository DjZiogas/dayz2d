﻿namespace DayZ2D
{
    public enum Layers
    {
        Background = 0,
        Prop = 10,
        BigProp = 15,
        PropOnProp = 20,
        Player = 500,
        Zombie = 510,
        Roof = 600,
        Tree = 610,
        Hud = 990,
        Cursor = 1000
    }

    public class DrawLayer
    {
        public static float GetLayer(Layers layer)
        {
            return (float) layer/1000;
        }
    }
}