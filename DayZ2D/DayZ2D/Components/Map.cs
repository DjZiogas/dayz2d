﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Autofac;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components
{
    public enum Colides
    {
        none,
        colides
    }

    public enum MapLayer
    {
        Background,
        Foreground
    }

    [Serializable]
    public struct VectorInt
    {
        public int X;
        public int Y;
    }

    [Serializable]
    public class Map : ICloneable
    {
        [NonSerialized]
        private Texture2D colTexture;
        private VectorInt _size;
        private Tile[][] _background;
        private Tile[][] _foreground;
        private Colides[][] _collisions;

        public Map(Vector2 size, Vector2 defTile)
        {
            _size = new VectorInt();
            _size.X = (int)size.X;
            _size.Y = (int)size.Y;
            Generate(defTile);
        }

        public void GenerateCollisions()
        {
            var colManager = DependencyInjectionConfiguration.Container.Resolve<CollisionManager>();
            for (var x = 0; x < _size.X; x++)
            {
                for (var y = 0; y < _size.Y; y++)
                {
                    if (_collisions[x][y] == Colides.colides)
                    {
                        colManager.AddCollider(new MapTileColider(new Vector2(x * 32, y * 32)));                               
                    }
                }
            }
        }

        public Vector2 Size
        {
            get { return new Vector2(_size.X,_size.Y); }
        }

        public void SetCollision(Vector2 pos, Colides collision)
        {
            var x = (int)Math.Floor(pos.X / 32);
            var y = (int)Math.Floor(pos.Y / 32);
            x = (int)MathHelper.Clamp(x, 0, _size.X - 1);
            y = (int)MathHelper.Clamp(y, 0, _size.Y - 1);
            _collisions[x][y] = collision;
        }

        public void SetTile(Vector2 pos, Vector2 tile, MapLayer layer)
        {
            var x = (int)Math.Floor(pos.X / 32);
            var y = (int)Math.Floor(pos.Y / 32);
            x = (int)MathHelper.Clamp(x, 0, _size.X - 1);
            y = (int)MathHelper.Clamp(y, 0, _size.Y - 1);
            if (layer == MapLayer.Background)
            {
                _background[x][y].SheetX = (int) tile.X;
                _background[x][y].SheetY = (int) tile.Y;
            }
            else
            {
                _foreground[x][y].SheetX = (int)tile.X;
                _foreground[x][y].SheetY = (int)tile.Y;
            }
        }

        public void Generate(Vector2 defTile)
        {
            _background = new Tile[_size.X][];
            _foreground = new Tile[_size.X][];
            _collisions = new Colides[_size.X][];
            for (var i = 0; i < _size.X; i++)
            {
                _background[i] = new Tile[_size.Y];
                _foreground[i] = new Tile[_size.Y];
                for (int k = 0; k < _size.Y; k++)
                {
                    _background[i][k].SheetX = (int)defTile.X;
                    _background[i][k].SheetY = (int)defTile.Y;
                    _foreground[i][k].SheetX = 30;
                    _foreground[i][k].SheetY = 30;
                }
                _collisions[i] = new Colides[_size.Y];
            }
        }

        public void LoadContent(ContentManager content)
        {
            colTexture = content.Load<Texture2D>("Images/32");
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D spriteSheet, bool showCollisions)
        {
            for (int i = 0; i < _size.X; i++)
            {
                for (int j = 0; j < _size.Y; j++)
                {
                    spriteBatch.Draw(spriteSheet,
                        new Vector2(i * 32 + 16, j * 32 + 16), 
                        new Rectangle(_background[i][j].SheetX * 32, _background[i][j].SheetY * 32, 32, 32),
                        Color.White,
                        _background[i][j].Rotation,
                        new Vector2(16),
                        Vector2.One, 
                        SpriteEffects.None,
                        DrawLayer.GetLayer(Layers.Background) );

                    spriteBatch.Draw(spriteSheet,
                        new Vector2(i * 32 + 16, j * 32 + 16),
                        new Rectangle(_foreground[i][j].SheetX * 32, _foreground[i][j].SheetY * 32, 32, 32),
                        Color.White,
                        _background[i][j].Rotation,
                        new Vector2(16),
                        Vector2.One,
                        SpriteEffects.None,
                        DrawLayer.GetLayer(Layers.Tree));
                }
            }

            if (showCollisions)
            {
                for (int i = 0; i < _size.X; i++)
                {
                    for (int j = 0; j < _size.Y; j++)
                    {
                        if (_collisions[i][j] == Colides.colides)
                        {
                            if (colTexture != null)
                            {
                            spriteBatch.Draw(colTexture, new Vector2(i * 32, j * 32), new Rectangle(0,0,32,32),new Color(new Vector4(0.5f)),0f,Vector2.Zero, 1f,SpriteEffects.None, DrawLayer.GetLayer(Layers.Cursor));
                            }
                        }
                    }
                }
            }
        }
        public void Save(string filePath)
        {
            BinaryFormatter formater = new BinaryFormatter();
            FileStream stream = new FileStream(filePath, FileMode.Create);
            formater.Serialize(stream,this);
            stream.Close();
        }

        public static object Load(string filePath)
        {
            BinaryFormatter formater = new BinaryFormatter();
            using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                return formater.Deserialize(stream);
            }
        }

        public object Clone()
        {
            Map newMap = new Map(new Vector2(_size.X,_size.Y), Vector2.One);
            newMap.colTexture = colTexture;
            for (int i = 0; i < _size.X; i++)
            {
                for (int j = 0; j < _size.Y; j++)
                {
                    newMap._background[i][j] = _background[i][j];
                    newMap._foreground[i][j] = _foreground[i][j];
                    newMap._collisions[i][j] = _collisions[i][j];
                }
            }
            return newMap;
        }
    }
}
