﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Components.Collisions;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DayZ2D.Components
{
    public class Player : ICollidableObject
    {
        private float _speed;
        private float _rotation;
        private Vector2 _lookPosition;
        private float _accel = 200f;
        private float _sprintSpeed;
        private float _moveSpeed;
        private Vector2 _direction;
        private AnimatedImage _image;
        public IShape Shape { get; set; }
        public bool Movable { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 PreviousPosition { get; set; }
        public Player(float speed, string image, Vector2 position, float scale)
        {
            Shape = new Circle(position);
            Movable = true;
            _speed = speed;
            _image = new AnimatedImage(image,position);
            _image.Scale = scale;
            _image.Layer = Layers.Player;
            Position = PreviousPosition = position;
        }

        public void LoadContent(ContentManager content)
        {
            _image.LoadContent(content);
        }

        public void Update(GameTime gameTime)
        {
            PreviousPosition = Position;

            var lookVector = _lookPosition - Position;
            lookVector.Normalize();
            _rotation = (float)(Math.Atan2(lookVector.Y, lookVector.X) / 1);

            if (InputManager.Instance.KeyHolding(Configuration.Instance.Controls.Sprint))
            {
                _sprintSpeed += _accel * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else
            {
                _sprintSpeed -= _accel * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            _sprintSpeed = MathHelper.Clamp(_sprintSpeed, 0f, _speed);

            bool moving = false;
            if (InputManager.Instance.KeyHolding(Configuration.Instance.Controls.WalkLeft))
            {
                moving = true;
                _direction.X = -1f;
            }
            else _direction.X = 0f;
            if (InputManager.Instance.KeyHolding(Configuration.Instance.Controls.WalkRight))
            {
                moving = true;
                _direction.X = 1f;
            }
            if (InputManager.Instance.KeyHolding(Configuration.Instance.Controls.WalkUp))
            {
                moving = true;
                _direction.Y = -1f;
            }
            else _direction.Y = 0f;
            if (InputManager.Instance.KeyHolding(Configuration.Instance.Controls.WalkDown))
            {
                moving = true;
                _direction.Y = 1f;
            }

            if (moving)
            {
                _moveSpeed += _accel * (float)gameTime.ElapsedGameTime.TotalSeconds;
            }
            else
            {
                _moveSpeed -= _accel * (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (_moveSpeed <= 0.0f)
                {
                    _direction = new Vector2(0);
                }
            }
            
            _moveSpeed = MathHelper.Clamp(_moveSpeed, 0f, _speed);

            //_direction = Rotate(_direction, _rotation+MathHelper.Pi/2);
            Position += (_moveSpeed + _sprintSpeed) * (_direction)* (float)gameTime.ElapsedGameTime.TotalSeconds;
            Shape.Position = Position;
            ScreenManager.Instance.Camera.CenterPos = Position;

            _image.Rotation = _rotation;
            _image.Position = Position;
            _image.Update(gameTime);
        }

        public Vector2 Rotate(Vector2 vector, float radians)
        {
            var x = vector.X * Math.Cos(radians) - vector.Y * Math.Sin(radians);
            var y = vector.X * Math.Sin(radians) + vector.Y * Math.Cos(radians);
            return new Vector2((float)x,(float)y);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var mousePosition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            _lookPosition = ScreenManager.Instance.Camera.ScreenToWorlSpace(mousePosition);
            _image.Draw(spriteBatch);
        }

        
        public bool Intersects(ICollidableObject other)
        {
            return Shape.IntersectVisit(other.Shape);
        }
    }
}
