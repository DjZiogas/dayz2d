﻿using DayZ2D.Components.AI;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;

namespace DayZ2D.Components.Factories
{
    public interface IBaseEnemyFactory
    {

        IBaseEnemy Spawn(EnemyTypes type, Vector2 position);
    }
}