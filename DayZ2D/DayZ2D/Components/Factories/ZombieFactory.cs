﻿using Autofac;
using DayZ2D.Components.AI;
using DayZ2D.Components.Collisions;
using DayZ2D.Helpers;
using DayZ2D.Logger;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;

namespace DayZ2D.Components.Factories
{
    public class ZombieFactory : IBaseEnemyFactory
    {
        private CollisionManager _colManager;
        private AbstractLogger _logger;

        public ZombieFactory(CollisionManager colManager, AbstractLogger log)
        {
            _colManager = colManager;
            _logger = log;
        }

        public IBaseEnemy Spawn(EnemyTypes type, Vector2 position)
        {
            IBaseEnemy zombie;
            switch (type)
            {
                case EnemyTypes.Lite:
                    zombie = new Zombie("Images/Zombie/Zombie_idle",position);
                    _logger.LogMessage(AbstractLogger.DEBUG, "Created lite zombie at " + position);
                    break;
                case EnemyTypes.Normal:
                    zombie = new Zombie("Images/Zombie/Zombie_move", position);
                    _logger.LogMessage(AbstractLogger.DEBUG, "Created normal zombie at " + position);
                    break;
                case EnemyTypes.Heavy:
                    zombie = new Zombie("Images/Zombie/Zombie_attack", position);
                    _logger.LogMessage(AbstractLogger.DEBUG, "Created heavy zombie at " + position);
                    break;
                default:
                    zombie = new Zombie("Images/Zombie/Zombie_idle", position);
                    _logger.LogMessage(AbstractLogger.DEBUG, "Created default zombie at " + position);
                    break;
            }

            zombie.LoadContent(ScreenManager.Instance.Content);
            _colManager.AddCollider((ICollidableObject)zombie);

            return zombie;
        }
    }
}