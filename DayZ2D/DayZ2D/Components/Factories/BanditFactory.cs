﻿using DayZ2D.Components.AI;
using DayZ2D.Components.AI.Builders;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;

namespace DayZ2D.Components.Factories
{
    public class BanditFactory : IBaseEnemyFactory
    {
        private CollisionManager _colManager;
        private BanditBuilder _builder;

        public BanditFactory(CollisionManager colManager, BanditBuilder builder)
        {
            _colManager = colManager;
            _builder = builder;

        }

        public IBaseEnemy Spawn(EnemyTypes type, Vector2 position)
        {
            switch (type)
            {
                case EnemyTypes.Lite:
                    return _builder.CreateLiteBandit();
                case EnemyTypes.Normal:
                    return _builder.CreateNormalBandit();
                case EnemyTypes.Heavy:
                    return _builder.CreateHeavyBandit();
                default: return new Bandit(position);
            }
        }
    }
}