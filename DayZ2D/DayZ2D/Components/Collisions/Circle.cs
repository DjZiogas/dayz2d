﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rect = Microsoft.Xna.Framework.Rectangle;

namespace DayZ2D.Components.Collisions
{


    public class Circle : IShape
    {
        public Circle(Vector2 position, int radius = 14)
        {
            Position = position;
            Radius = radius;
        }

        public Vector2 Position { get; set; }
        public int Radius { get; set; }

        public bool IntersectVisit(IShape other)
        {
            return other.Intersect(this);
        }

        public bool Intersect(Circle circle)
        {
            if (Vector2.Distance(Position, circle.Position) <= Radius + circle.Radius)
            {
                return true;
            }
            return false;
        }

        public bool Intersect(Rectangle rectangle)
        {
            
            var rect = new Rect((int)rectangle.Position.X, (int)rectangle.Position.Y,
                (int)rectangle.Dimensions.X, (int)rectangle.Dimensions.Y);
            var v = new Vector2(MathHelper.Clamp(Position.X, rect.Left, rect.Right),
                                MathHelper.Clamp(Position.Y, rect.Top, rect.Bottom));

            var direction = Position - v;
            var distanceSquared = direction.LengthSquared();

            return ((distanceSquared > 0) && (distanceSquared < Radius * Radius));
        }
    }
}
