﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rect = Microsoft.Xna.Framework.Rectangle;

namespace DayZ2D.Components.Collisions
{
    public class Rectangle :IShape
    {
        public Rectangle(Vector2 position, Vector2 dimensions)
        {
            Position = position;
            Dimensions = dimensions;
        }

        public Vector2 Position { get; set; }
        public Vector2 Dimensions { get; set; }


        public bool IntersectVisit(IShape other)
        {
            return other.Intersect(this);
        }

        public bool Intersect(Circle circle)
        {
            var rect = new Rect((int)Position.X, (int)Position.Y, (int)Dimensions.X, (int)Dimensions.Y);
            var v = new Vector2(MathHelper.Clamp(circle.Position.X, rect.Left, rect.Right),
                                MathHelper.Clamp(circle.Position.Y, rect.Top, rect.Bottom));

            var direction = circle.Position - v;
            var distanceSquared = direction.LengthSquared();

            return ((distanceSquared > 0) && (distanceSquared < circle.Radius * circle.Radius));
        }

        public bool Intersect(Rectangle rectangle)
        {
            var rect1 = new Rect((int)Position.X, (int)Position.Y, (int)Dimensions.X, (int)Dimensions.Y);
            var rect2 = new Rect((int)rectangle.Position.X, (int)rectangle.Position.Y, 
                (int)rectangle.Dimensions.X, (int)rectangle.Dimensions.Y);

            if (Rect.Intersect(rect1, rect2) != Rect.Empty)
            {
                return true;
            }
            return false;
        }
    }
}
