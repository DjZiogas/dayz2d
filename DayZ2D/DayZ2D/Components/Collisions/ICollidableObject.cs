﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DayZ2D.Components.Collisions
{
    public interface ICollidableObject
    {

        IShape Shape { get; set; }
        bool Movable { get; set; }
        Vector2 Position { get; set; }
        Vector2 PreviousPosition { get; set; }

        bool Intersects(ICollidableObject other);
        
    }
}
