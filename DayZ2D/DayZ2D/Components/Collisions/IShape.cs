﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components.Collisions
{
    public interface IShape
    {
        Vector2 Position { get; set; }

        bool IntersectVisit(IShape other);
        
        bool Intersect(Circle circle);
        bool Intersect(Rectangle rectangle);
    }
}
