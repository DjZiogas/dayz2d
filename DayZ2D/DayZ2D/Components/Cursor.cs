﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DayZ2D.Components
{
    public class Cursor
    {
        private float scale;
        private float rotationSpeed;
        private float rotation;
        private Texture2D texture;
        private string image;
        private Vector2 possition;
        public bool IsEnabled { get; set; }
        public Cursor(string image, float scale = 0.5f, float rotSpeed = 2f, bool enabled = false)
        {
            this.image = image;
            this.scale = scale;
            rotationSpeed = rotSpeed;
            IsEnabled = enabled;
        }

        public void LoadContent(ContentManager content)
        {
            texture = content.Load<Texture2D>(image);
        }

        public void Update(GameTime gameTime)
        {
            rotation += rotationSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            possition = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsEnabled)
            {
                spriteBatch.Draw(texture, possition, null, Color.White, rotation, new Vector2(64), scale,
                    SpriteEffects.None, DrawLayer.GetLayer(Layers.Cursor));
            }
        }
    }
}
