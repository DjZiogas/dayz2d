﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DayZ2D.Components.AI.Strategies
{
    public interface IStrategy
    {
        void Move();
    }
}
