﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Components.Items;
using Microsoft.Xna.Framework;

namespace DayZ2D.Components.AI.Builders
{
    public class BanditBuilder
    {
        public IBaseEnemy CreateLiteBandit()
        {
            var bandit = new Bandit(Vector2.Zero);
            IItem pistol = new Pistol();
            bandit.AddItem(pistol);
            return bandit;
        }
        public IBaseEnemy CreateNormalBandit()
        {
            var bandit = new Bandit(Vector2.Zero);
            IItem pistol = new Pistol();
            IItem helmet = new Helmet();

            bandit.AddItem(pistol);
            bandit.AddItem(helmet);
            return bandit;
        }
        public IBaseEnemy CreateHeavyBandit()
        {
            var bandit = new Bandit(Vector2.Zero);
            IItem shotgun = new Shotgun();
            IItem helmet = new Helmet();
            IItem chestPlate = new ChestArmor();

            bandit.AddItem(shotgun);
            bandit.AddItem(helmet);
            bandit.AddItem(chestPlate);
            return bandit;
        }
    }
}
