﻿using System.Collections;
using System.Collections.Generic;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components.AI
{
    public interface IBaseEnemy
    {
        void LoadContent(ContentManager contentManager);
        void Update(GameTime gameTime);
        void Draw(SpriteBatch spriteBatch);
        void Move(Vector2 location);
    }
}