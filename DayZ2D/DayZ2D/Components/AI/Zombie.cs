﻿using System;
using System.Collections.Generic;
using DayZ2D.Components.AI.Strategies;
using DayZ2D.Components.Collisions;
using DayZ2D.Helpers;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components.AI
{
    public class Zombie : IBaseEnemy, ICollidableObject
    {
        private IStrategy _strategy;
        private AnimatedImage _image;

        private float temp;

        public Zombie(string image, Vector2 position)
        {
            Shape = new Circle(position);
            Position = PreviousPosition = position;
            Movable = true;
            _image = new AnimatedImage(image, position);
        }

        public void LoadContent(ContentManager contentManager)
        {
            _image.LoadContent(contentManager);
            _image.Layer = Layers.Zombie;
        }

        public void UnloadContent()
        {

        }

        public void Update(GameTime gameTime)
        {
            PreviousPosition = Position;
            temp += 10*(float)gameTime.ElapsedGameTime.TotalSeconds;
            if (temp > 10)
            {
                temp = 0;
                _image.ChangeImage("Images/Zombie/Zombie_attack",1);
            }
            _image.Update(gameTime);

            //_strategy.Move();
            Shape.Position = Position;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            _image.Draw(spriteBatch);
        }

        public void Move(Vector2 location)
        {
            throw new NotImplementedException();
        }

        public IShape Shape { get; set; }
        public bool Movable { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 PreviousPosition { get; set; }
        public bool Intersects(ICollidableObject other)
        {
            return Shape.IntersectVisit(other.Shape);
        }
    }
}