﻿using System.Collections.Generic;
using DayZ2D.Components.Collisions;
using DayZ2D.Components.Items;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components.AI
{
    public class Bandit : IBaseEnemy, ICollidableObject
    {
        protected List<IItem> items;

        public Bandit(Vector2 position)
        {
            Shape = new Circle(position, 50);
            Position = position;
            Movable = true;
            items = new List<IItem>();
        }

        public void LoadContent(ContentManager contentManager)
        {
        }

        public void UnloadContent()
        {
        }

        public void Update(GameTime gameTime)
        {
            Shape.Position = Position;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
        }

        public void Move(Vector2 location)
        {
            throw new System.NotImplementedException();
        }

        public void AddItem(IItem item)
        {
            items.Add(item);
        }

        public IShape Shape { get; set; }
        public bool Movable { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 PreviousPosition { get; set; }
        public bool Intersects(ICollidableObject other)
        {
            return Shape.IntersectVisit(other.Shape);
        }
    }
}