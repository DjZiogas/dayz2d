﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Components.Collisions;
using Microsoft.Xna.Framework;
using Rectangle = DayZ2D.Components.Collisions.Rectangle;

namespace DayZ2D.Components
{
    class MapTileColider : ICollidableObject
    {
        public MapTileColider(Vector2 position)
        {
            Shape = new Rectangle(position,new Vector2(32,32));
            Movable = false;
            Position = position;
        }

        public IShape Shape { get; set; }
        public bool Movable { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 PreviousPosition { get; set; }
        public bool Intersects(ICollidableObject other)
        {
            return Shape.IntersectVisit(other.Shape);
        }
    }
}
