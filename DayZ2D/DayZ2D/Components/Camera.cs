﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components
{
    public class Camera
    {
        private Matrix _transform;
        private float _rotation;
        private float _zoom;

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value;
                if (_zoom < 0.1f) _zoom = 0.1f;
            }
        }

        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }
        public Vector2 CenterPos
        {
            get { return _pos; }
            set
            {
                _pos.X = value.X - Configuration.Instance.Settings.ScreenWidth / (2 * _zoom);
                _pos.Y = value.Y - Configuration.Instance.Settings.ScreenHeight / (2 * _zoom);
            }
        }

        private Vector2 _pos;

        public Camera()
        {
            _pos = Vector2.Zero;
            _rotation = 0.0f;
            _zoom = 1.0f;
        }

        public void Move(Vector2 amount)
        {
            _pos += amount;
        }

        public Matrix getTransform(GraphicsDevice graphics)
        {
            _transform =
                Matrix.CreateTranslation(new Vector3(-_pos.X, -_pos.Y, 0f)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(0,
                                             0, 0));
            return _transform;
        }

        public Vector2 WorldToScreenSpace(Vector2 postions)
        {
            return Vector2.Transform(postions, _transform);
        }

        public Vector2 ScreenToWorlSpace(Vector2 position)
        {
            return Vector2.Transform(position, Matrix.Invert(_transform));
        }
    }
}
