﻿using Autofac;
using DayZ2D.Components.Factories;
using DayZ2D.Helpers;

namespace DayZ2D.Components
{
    public class EnemyProducer
    {
        public static IBaseEnemyFactory CreateEnemy(Enemy enemy)
        {
            switch (enemy)
            {
                case Enemy.Zombie:
                    return DependencyInjectionConfiguration.Container.Resolve<ZombieFactory>();
                case Enemy.Bandit:
                    return DependencyInjectionConfiguration.Container.Resolve<BanditFactory>();
                default:
                    return DependencyInjectionConfiguration.Container.Resolve<ZombieFactory>();
            }
        }
    }
}
