﻿using System.Collections.Generic;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components.GUI
{
    public class Menu
    {
        private ContentManager _content;
        public IList<Button> MenuButtons { get; private set; }
        private bool _hiden;
        private string _title;
        private SpriteFont _titleFont;
        private bool _unhiding;
        private float _alpha;

        public Menu(IList<Button> menuButtons, bool hiden, string title)
        {
            MenuButtons = menuButtons;
            _hiden = hiden;
            _title = title;
        }

        public void AddButton(Button btn)
        {
            MenuButtons.Add(btn);
            btn.LoadContent(_content);
        }

        public void RemoveButton(Button btn)
        {
            MenuButtons.Remove(btn);
        }

        public void SetHidden(bool hiden)
        {
            _hiden = hiden;
            _unhiding = !hiden;
            if(hiden)
                foreach (var menuButton in MenuButtons)
                {
                    menuButton.Enabled = false;
                }
            
        }
        public bool IsHidden { get { return _hiden;} }

        public void LoadContent(ContentManager content)
        {
            _titleFont = content.Load<SpriteFont>("Fonts/MenuTitle");
            var pos = new Vector2(0.2f,0.2f);
            _content = content;
            foreach (var menuButton in MenuButtons)
            {
                menuButton.LoadContent(_content);
                menuButton.Position = pos;
                pos.Y += 0.1f;
            }
        }

        public void Update(GameTime gameTime)
        {
            if (_hiden) return;
            if (_unhiding)
            {
                _alpha += 1*(float)gameTime.ElapsedGameTime.TotalSeconds;
                if (_alpha >= 0.5)
                {
                    _unhiding = false;
                    foreach (var menuButton in MenuButtons)
                    {
                        menuButton.Enabled = !_hiden;
                    }
                }
            }
            foreach (var menuButton in MenuButtons)
            {
                menuButton.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_hiden) return;
            spriteBatch.DrawString(_titleFont,_title,new Vector2(20), Color.White);
            foreach (var menuButton in MenuButtons)
            {
                menuButton.Draw(spriteBatch);
            }
        }
    }
}
