﻿using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DayZ2D.Components.GUI
{
    public class Button
    {
        private ICommand _command;
        private Vector2 _position;
        private Vector2 _absPosition;
        private Vector2 _dimensions;
        private string _text;
        private Texture2D _texture;
        private SpriteFont _spriteFont;
        private bool _selected;

        public bool Enabled { get; set; }

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public Vector2 Dimensions
        {
            get { return _dimensions; }
            set { _dimensions = value; }
        }



        public Button(ICommand command, string text)
        {
            _command = command;
            Enabled = true;
            _text = text;

            _position = new Vector2(0f);
            _dimensions = new Vector2(200, 50);
            CalculateAbsolutePosition();
        }

        public void LoadContent(ContentManager content)
        {
            _texture = content.Load<Texture2D>("Images/32");
            _spriteFont = content.Load<SpriteFont>("Fonts/28dayslater");

        }

        public void Update(GameTime gameTime)
        {
            _selected = false;
            if (!Enabled) return;
            CalculateAbsolutePosition();
            var mouse = Mouse.GetState();
            var x = mouse.X;
            var y = mouse.Y;
            if (x > _absPosition.X && y > _absPosition.Y && x < _absPosition.X + _dimensions.X && y < _absPosition.Y + _dimensions.Y)
            {
                _selected = true;
            }


            if (InputManager.Instance.MouseReleased())
            {

                if (_selected)
                {
                    _command.Execute(new object[]{_text});
                }
            }

        }

        private void CalculateAbsolutePosition()
        {
            _absPosition = new Vector2()
            {
                X = ScreenManager.Instance.Viewport.Width * _position.X - (_dimensions.X / 2),
                Y = ScreenManager.Instance.Viewport.Height * _position.Y - (_dimensions.Y / 2)
            };
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled) return;

            var color = Color.White;
            if (_selected)
                color = Color.GreenYellow;

            spriteBatch.Draw(_texture, new Rectangle((int)_absPosition.X, (int)_absPosition.Y, (int)_dimensions.X, (int)_dimensions.Y), new Rectangle(0, 0, 16, 16), color, 0f, Vector2.Zero, SpriteEffects.None, DrawLayer.GetLayer(Layers.Hud));
            spriteBatch.DrawString(_spriteFont, _text, new Vector2(_absPosition.X + 10, _absPosition.Y + 10), Color.White, 0f, Vector2.Zero, Vector2.One, SpriteEffects.None, DrawLayer.GetLayer(Layers.Hud + 1));
        }


    }
}
