﻿using System;
using DayZ2D.GameScreens;

namespace DayZ2D.Components.GUI.Commands
{
    public class PauseCommand : ICommand
    {
        private BaseGameScreen menu;

        public PauseCommand(BaseGameScreen menu)
        {
            this.menu = menu;
        }
        public void Execute(Object [] args)
        {
            ((GameplayScreen)menu).Pause();
        }
    }
}