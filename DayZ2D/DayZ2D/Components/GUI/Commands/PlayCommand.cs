﻿using System;
using DayZ2D.GameScreens;

namespace DayZ2D.Components.GUI.Commands
{
    class PlayCommand : ICommand
    {
        private MenuScreen menu;

        public PlayCommand(MenuScreen menu)
        {
            this.menu = menu;
        }

        public void Execute(Object[] args)
        {
            menu.Play();
        }
    }
}
