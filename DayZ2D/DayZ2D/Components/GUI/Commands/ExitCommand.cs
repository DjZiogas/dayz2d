﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DayZ2D.GameScreens;

namespace DayZ2D.Components.GUI.Commands
{
    class ExitCommand : ICommand
    {
        private MenuScreen menu;

        public ExitCommand(MenuScreen menu)
        {
            this.menu = menu;
        }

        public void Execute(Object[] args)
        {
            menu.Exit();
        }
    }
}
