﻿using System;
using DayZ2D.GameScreens;

namespace DayZ2D.Components.GUI.Commands
{
    class SettingsCommand : ICommand
    {
        private MenuScreen menu;

        public SettingsCommand(MenuScreen menu)
        {
            this.menu = menu;
        }

        public void Execute(Object[] args)
        {
            menu.Settings();
        }
    }
}