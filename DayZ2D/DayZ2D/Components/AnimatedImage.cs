﻿using System;
using System.Dynamic;
using DataTypes;
using DayZ2D.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DayZ2D.Components
{
    public class AnimatedImage
    {
        private Texture2D _texture;
        private string _imagePath;
        private string _previosImagePath;
        private int _loopsLeft;
        private bool isCountedLooping = false;
        private Vector2 _position;
        private float _animationTime;
        private float _rotation;
        private float _scale = 0.2f;
        private Layers _layer = 0;
        private SpriteSheetInfo _info;
        private Vector2 _grid;

        public Vector2 Origin {get; private set; }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }
        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public float Scale
        {
            get { return _scale; }
            set { _scale = value; }
        }

        public Layers Layer
        {
            get { return _layer; }
            set { _layer = value; }
        }

        public AnimatedImage(string imagePath, Vector2 position)
        {
            _imagePath = imagePath;
            _position = position;
        }

        public void ChangeImage(string imagePath, int loops)
        {
            if (loops > 0)
            {
                _previosImagePath = _imagePath;
                _imagePath = imagePath;
                LoadContent(ScreenManager.Instance.Content);
                _loopsLeft = loops+1;
                isCountedLooping = true;
            }
            else
            {
                _imagePath = imagePath;
                LoadContent((ScreenManager.Instance.Content));
            }
        }

        private void RevertImage()
        {
            _imagePath = _previosImagePath;
            LoadContent(ScreenManager.Instance.Content);
        }

        public void LoadContent(ContentManager content)
        {
            _texture = content.Load<Texture2D>(_imagePath);
            _info = content.Load<SpriteSheetInfo>(_imagePath + "_info");
            _grid = new Vector2(_texture.Width/_info.X, _texture.Height/_info.Y);
            Origin = new Vector2(_info.X/2,_info.Y/2);
        }

        public void UnloadContent()
        {

        }

        public void Update(GameTime gameTime)
        {
            _animationTime += 16 * (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (_animationTime > _info.Frames)
            {
                _animationTime = 0;
                if(isCountedLooping)
                    _loopsLeft--;
            }
            if (isCountedLooping && _loopsLeft == 0)
            {
                RevertImage();
                isCountedLooping = false;
            }

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var _currentFrame = Math.Floor(_animationTime);
            var x = (_currentFrame % _grid.X) * _info.X;
            var y = Math.Floor(_currentFrame / _grid.X) * _info.Y;
            spriteBatch.Draw(_texture, _position, new Rectangle((int)x, (int)y, _info.X, _info.Y), Color.White, _rotation, Origin, new Vector2(_scale), SpriteEffects.None, DrawLayer.GetLayer(Layers.Player));
        }
    }
}