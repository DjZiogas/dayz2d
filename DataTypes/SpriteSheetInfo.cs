﻿namespace DataTypes
{
    public class SpriteSheetInfo
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Frames { get; set; }
    }
}
